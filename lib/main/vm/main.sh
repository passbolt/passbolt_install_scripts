main() {
  init_config
  validate_os 'debian'
  __prompt_passbolt_hostname 'passbolt_hostname'
  __prompt_ssl 'ssl_auto' \
               'ssl_manual' \
               'ssl_none' \
               'ssl_certificate' \
               'ssl_privkey' \
               'letsencrypt_email'
  setup_nginx
}

main "$@" 2>&1 | tee -a ssl_install.log
